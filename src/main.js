import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router'
import Routes from './routes.js'
import Vuetify from 'vuetify'
import VeeValidate from 'vee-validate';
import moment from 'moment'
import 'vuetify/dist/vuetify.min.css'

Vue.use(VueRouter);
Vue.use(VeeValidate)
Vue.use(Vuetify, {
  theme: {
    primary: '#000000',
  }
});

const router = new VueRouter({
  routes: Routes,
  mode: 'history'
});

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY HH:mm')
  }
})

new Vue({
  el: '#app',
  render: h => h(App),
  router: router
})
