import axios from 'axios';

const API_URL = 'http://localhost:8080/getWorkTimeSales'

export default {
    getWorktimeSales(wtId) {
        return axios.post(API_URL, null, {
            params: {
                'id': wtId
            },
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        });
    }
}