import axios from 'axios';

const API_URL = 'http://localhost:8080/userDetails'

export default {
    details() {
        return axios.post(API_URL, null, {
            headers: { 
                'Authorization': localStorage.getItem('token')
            }
        });
    }
}