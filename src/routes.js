import Login from './login/Login.vue'
import Dashboard from './dashboard/Dashboard.vue'
import UserPanel from './userpanel/UserPanel.vue'
import Sales from './sales/Sales.vue'
import WorkTimeReports from './reports/workTimeReports/WorkTimeReports.vue'
import SalesReports from './reports/salesReports/SalesReports.vue'
import Diagrams from './reports/diagrams/Diagrams.vue'
import AddEmployee from './admin/addEmployee/AddEmployee.vue'
import AddProduct from './admin/addProduct/AddProduct.vue'


export default [
    { path: '/', component: Login },
    { path: '/dashboard', component: Dashboard },
    { path: '/userpanel', component: UserPanel },
    { path: '/sales/:id', component: Sales },
    { path: '/worktimereports', component: WorkTimeReports },
    { path: '/salesreports', component: SalesReports },
    { path: '/diagrams', component: Diagrams },
    { path: '/addemployee', component: AddEmployee },
    { path: '/addproduct', component: AddProduct } 
]