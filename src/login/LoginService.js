import axios from 'axios';

const API_URL = 'http://localhost:8080/login'
const API_URL2 = 'http://localhost:8080/getRoles'

export default {

    login(loginModel) {
        return axios.post(API_URL, loginModel);
    },

    getRoles() {
        return axios.get(API_URL2, {
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        })
    }
}

    
    
    
