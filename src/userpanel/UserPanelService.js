import axios from 'axios';

const API_URL = 'http://localhost:8080/userpanel'

export default {
    userDetails() {
        return axios.post(API_URL, null, {
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        });
    }
}