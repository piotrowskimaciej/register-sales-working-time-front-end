import axios from 'axios';

const API_URL_GE = 'http://localhost:8080/getAllEmployees'
const API_URL_GWT = 'http://localhost:8080/getEmployeeWorkTime'

export default {

    getEmployees() {
        return axios.post(API_URL_GE, null, {
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        });
    },

    getWorktimeSales(eId, days) {
        return axios.post(API_URL_GWT, null, {
            params: {
                'id': eId,
                'days': days
            },
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        });
    }
}