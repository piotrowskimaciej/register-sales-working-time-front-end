export const salesChartData = {
  type: 'bar',
  data: {
    labels: ['Accessories', 'Computers', 'Printers'],
    datasets: [
      { // one line graph
        label: 'Amount of sales',
        data: [],
        backgroundColor: [
          '#66BB6A', // Blue
          'salmon',
          '#78909C',
        ],
      },
    ]
  },
  options: {
    responsive: true,
    lineTension: 1,
    legend: {
      labels: {
        fontColor: 'white'
      }
    },
    scales: {
      xAxes: [{
        ticks: {
          fontColor: 'white'
        }
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          padding: 25,
          fontColor: 'white'
        }
      }]
    }
  }
}

export default salesChartData;