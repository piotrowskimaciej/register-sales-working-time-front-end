export const percentageSalesChartData = {
  type: 'doughnut',
  data: {
    labels: ['Accessories', 'Computers', 'Printers'],
    datasets: [
      { // one line graph
        data: [],
        backgroundColor: [
          '#66BB6A',
          '#FFA726',
          '#F4511E',
        ],
        borderColor: [
          '#424242',
          '#424242',
          '#424242',
          '#424242',
          '#424242',
          '#424242',
          '#424242',
          '#424242',
        ],
        borderWidth: 3
      },
    ]
  },
  options: {
    responsive: true,
    lineTension: 1,
    legend: {
      labels: {
        fontColor: 'white'
      }
    },
  }
}
  
export default percentageSalesChartData;