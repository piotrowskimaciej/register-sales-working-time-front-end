import axios from 'axios'

const API_URL_AP = 'http://localhost:8080/addNewProduct'

export default {
    addProduct(addProductModel) {
        return axios.post(API_URL_AP, addProductModel, {
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        })
    }
}