import axios from 'axios'

const API_URL_AE = 'http://localhost:8080/addNewUser'

export default {
    addEpmloyee(addEmployeeModel) {
        return axios.post(API_URL_AE, addEmployeeModel, {
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        })
    }
}