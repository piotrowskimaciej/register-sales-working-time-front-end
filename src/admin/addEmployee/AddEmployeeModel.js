export class AddEmployeeModel {
    constructor(login, name, surname, password, email, role) {
        this.name = name
        this.surname = surname
        this.login = login
        this.password = password
        this.email = email
        this.role = role
    }
}