import axios from 'axios';

const API_URL = 'http://localhost:8080/getWorkTime'
const API_URL_SW = 'http://localhost:8080/startWork'
const API_URL_FW = 'http://localhost:8080/endWork'
const API_URL_PDT = 'http://localhost:8080/getAllProducts'
const API_URL_SE = 'http://localhost:8080/saleProduct'


export default {
    worktimeDetails() {
        return axios.post(API_URL, null, {
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        });
    },

    startWork() {
        return axios.post(API_URL_SW, null, {
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        });
    },

    endWork(workEndModel) {
        return axios.post(API_URL_FW, workEndModel, {
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        });
    },

    getProducts() {
        return axios.post(API_URL_PDT, null, {
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        });
    },

    saleProduct(pId, wtId) {
        return axios.post(API_URL_SE, null, {
            params: {
                'pId': pId,
                'wId': wtId
            },
            headers: { 
                'Authorization': localStorage.getItem('token') 
            }
        });
    }
}